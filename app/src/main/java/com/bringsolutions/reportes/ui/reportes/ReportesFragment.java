package com.bringsolutions.reportes.ui.reportes;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.bringsolutions.reportes.BuildConfig;
import com.bringsolutions.reportes.R;
import com.bringsolutions.reportes.adaptadores.AdaptadorActividades;
import com.bringsolutions.reportes.objetos.Actividad;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import static android.app.Activity.RESULT_OK;

public class ReportesFragment extends Fragment {
	View view;
	TextView tvMuestraDireccion;
	ImageButton btnObtenerDireccion, btnFotoInicialUno,btnFotoInicialDos,btnFotoFinalUno,btnFotoFinalDos;
	ImageView vistaFotoInicialUno,vistaFotoInicialDos,vistaFotoFinalUno,vistaFotoFinalDos;
	TextView tvTextoFotosIniciales;
	TextView tvTextoFotosFinales;
	Switch swMostrarActividades,swMostrarActividadesExtras;
	
	private static final int SOLICITUD_FOTO_INICIAL_1 = 10;
	private static final int SOLICITUD_FOTO_INICIAL_2 = 11;
	private static final int SOLICITUD_FOTO_FINAL_1 = 12;
	private static final int SOLICITUD_FOTO_FINAL_2 = 13;
	
	private Uri uriImagenIncialUno;
	private Uri uriImagenIncialDos;
	private Uri uriImagenFinalUno;
	private Uri uriImagenFinalDos;
	
	LinearLayout linearActividades, linearDirecionRecibida,linearActividadesExtras;
	Button btnBuscarActividad;
	EditText cajaFolioActividad, cajaNumActExt;
	
	RecyclerView recyclerViewActividadesExtras;
	
	SignaturePad cuadroFirma;
	Button btnGuardarFirma, btnLimpiarFirma;
	
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_reportes, container, false);
		
		inicializarElementos();
		
		clicks();
		
		return view;
	}
	
	private void clicks() {
		
		swMostrarActividades.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					swMostrarActividades.setChecked(true);
					linearActividades.setVisibility(View.VISIBLE);
					
				}else{
					swMostrarActividades.setChecked(false);
					linearActividades.setVisibility(View.GONE);
				}
			}
		});
		
		
		
		swMostrarActividadesExtras.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					swMostrarActividadesExtras.setChecked(true);
					linearActividadesExtras.setVisibility(View.VISIBLE);
					
				}else{
					swMostrarActividadesExtras.setChecked(false);
					linearActividadesExtras.setVisibility(View.GONE);
				}
			}
		});
		
		
		
		btnBuscarActividad.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String cajaFolio = cajaFolioActividad.getText().toString();
				linearDirecionRecibida.setVisibility(View.VISIBLE);
				linearActividades.setVisibility(View.VISIBLE);
				swMostrarActividades.setChecked(true);
			}
		});
		
		btnObtenerDireccion.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				obtenerDireccion();
			}
		});
		
		btnFotoInicialUno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				obtenerFoto(1);
			}
		});
		
		
		
		btnFotoInicialDos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				obtenerFoto(2);
			}
		});
		
		btnFotoFinalUno.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {obtenerFoto(3);
			}
		});
		
		btnFotoFinalDos.setOnClickListener(new View.OnClickListener() {	@Override
			public void onClick(View v) {obtenerFoto(4);
			}
		});
		
		cajaNumActExt.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				try {
					if (cajaNumActExt.getText().toString() != null ) {
						int numActividades = Integer.parseInt(cajaNumActExt.getText().toString());
						generarTarjetasActividades(numActividades);
						
					}else if( cajaNumActExt.getText().toString() == ""){
						generarTarjetasActividades(0);
						
					}
					
				}catch (Exception e) {

				}
				
			}
		});
		
		cuadroFirma.setOnSignedListener(new SignaturePad.OnSignedListener() {
			@Override
			public void onStartSigning() {
			
			}
			
			@Override
			public void onSigned() {
				btnGuardarFirma.setEnabled(true);
				btnLimpiarFirma.setEnabled(true);
			}
			
			@Override
			public void onClear() {
				btnGuardarFirma.setEnabled(false);
				btnLimpiarFirma.setEnabled(false);
			}
		});
		
		btnGuardarFirma.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(getActivity(), "Firma guardada con éxito!", Toast.LENGTH_SHORT).show();
			}
		});
		
		btnLimpiarFirma.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				cuadroFirma.clear();
			}
		});
		
		
		
	
	}
	
	private void generarTarjetasActividades(int numActividades) {
		recyclerViewActividadesExtras.setLayoutManager(new LinearLayoutManager(getActivity()));
		AdaptadorActividades adapter;
		List<Actividad> ACTIVIDADES = new ArrayList<>();
		
		for (int i = 0; i < numActividades; i++) {
			String descripcion ="";
			String foto ="";
			
			ACTIVIDADES.add(new Actividad(descripcion, foto));
			adapter = new AdaptadorActividades(ACTIVIDADES, getActivity());
			recyclerViewActividadesExtras.setAdapter(adapter);
			
		}
	
	}
	
	private void obtenerFoto(int num_foto) {
		verificarPermisosCamaraMemoria(num_foto);
		
	}
	
	private void verificarPermisosCamaraMemoria(int num_foto) {
		if (getActivity() != null){
			if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
				ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,}, 2);
				ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 3);
			} else {
				abrirCamara(num_foto);

			}
		}


	}

	private void abrirCamara(int foto) {
		
		if(foto==1){
			
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.setType("image/*");
			i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenIncialUno);
			startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_INICIAL_1);
			
			
			
			/*
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
			String currentDateandTime = sdf.format(new Date());

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_uno"+currentDateandTime+".jpg");

			if (getActivity() != null) {
				uriImagenIncialUno = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialUno);
				startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_1);
			}else{
				Toast.makeText(getActivity(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
			}
*/

		}else if(foto==2){
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.setType("image/*");
			i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenIncialDos);
			startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_INICIAL_2);
/*
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
			String currentDateandTime = sdf.format(new Date());
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotoinicial_dos"+currentDateandTime+".jpg");
			if (getActivity() != null){
				uriImagenIncialDos = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenIncialDos);
				startActivityForResult(intent, SOLICITUD_FOTO_INICIAL_2);
			}

			*/
		}else if(foto==3){
			
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.setType("image/*");
			i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenFinalUno);
			startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_FINAL_1);
			
			/*
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
			String currentDateandTime = sdf.format(new Date());
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_uno"+currentDateandTime+".jpg");
			if (getActivity() != null){
				uriImagenFinalUno = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalUno);
				startActivityForResult(intent, SOLICITUD_FOTO_FINAL_1);
			}

			*/
		}else if(foto==4){
			
			Intent i = new Intent(Intent.ACTION_GET_CONTENT);
			i.setType("image/*");
			i.putExtra(Intent.EXTRA_LOCAL_ONLY,uriImagenFinalDos);
			startActivityForResult(Intent.createChooser(i,"Selecciona una foto"),SOLICITUD_FOTO_FINAL_2);
			
/*
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
			String currentDateandTime = sdf.format(new Date());
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			File fotoArchivo = new File(Environment.getExternalStorageDirectory(), "fotofinal_dos"+currentDateandTime+".jpg");
			if (getActivity() != null){
				uriImagenFinalDos = FileProvider.getUriForFile(getActivity(),BuildConfig.APPLICATION_ID + ".fileprovider", fotoArchivo);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uriImagenFinalDos);
				startActivityForResult(intent, SOLICITUD_FOTO_FINAL_2);
			}
*/
			
		}
		
		
		
		
	}

	private void obtenerDireccion() {
		verificarPermisosUbicacionTelefono();
	}
	
	private void verificarPermisosUbicacionTelefono() {
		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1);
		} else {
			extraerUbicacion();
		}
		
	}
	
	private void extraerUbicacion() {
		LocationManager mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!gpsEnabled) {
			Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(settingsIntent);
		}
		if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
			return;
		}

		 Location location = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		try {
			Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
			//No estoy recibiendo ningun dato en location
			List<Address> list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
			if (!list.isEmpty()) {
				Address DirCalle = list.get(0);
				Toast.makeText(getActivity(), ""+DirCalle.getAddressLine(0), Toast.LENGTH_SHORT).show();
				tvMuestraDireccion.setText(DirCalle.getAddressLine(0));
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error gps moto", e.getLocalizedMessage());
		}
		
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == SOLICITUD_FOTO_INICIAL_1 && resultCode == RESULT_OK) {
			try {
			Uri u = data.getData();
			Bitmap bitmap = decodificarBitmap(getActivity(), u);
			btnFotoInicialUno.setVisibility(View.GONE);
			vistaFotoInicialUno.setVisibility(View.VISIBLE);
			vistaFotoInicialUno.setImageBitmap(bitmap);
			tvTextoFotosIniciales.setText("Pulsa en alguna foto para tomar otra nueva");
			
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			/*
			try {
				Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialUno);
				btnFotoInicialUno.setVisibility(View.GONE);
				vistaFotoInicialUno.setVisibility(View.VISIBLE);
				vistaFotoInicialUno.setImageBitmap(bitmap);
				tvTextoFotosIniciales.setText("Pulsa en alguna foto para tomar otra nueva");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			*/
		}else if (requestCode == SOLICITUD_FOTO_INICIAL_2 && resultCode == RESULT_OK) {
			try {
				Uri u = data.getData();
				Bitmap bitmap = decodificarBitmap(getActivity(), u);
				btnFotoInicialDos.setVisibility(View.GONE);
				vistaFotoInicialDos.setVisibility(View.VISIBLE);
				vistaFotoInicialDos.setImageBitmap(bitmap);
				tvTextoFotosIniciales.setText("Pulsa en alguna foto para tomar otra nueva");
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			/*
			try {
				Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenIncialDos);
				btnFotoInicialDos.setVisibility(View.GONE);
				vistaFotoInicialDos.setVisibility(View.VISIBLE);
				vistaFotoInicialDos.setImageBitmap(bitmap);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			*/
			
		}else if (requestCode == SOLICITUD_FOTO_FINAL_1 && resultCode == RESULT_OK) {
			
			try {
				Uri u = data.getData();
				Bitmap bitmap = decodificarBitmap(getActivity(), u);
				btnFotoFinalUno.setVisibility(View.GONE);
				vistaFotoFinalUno.setVisibility(View.VISIBLE);
				vistaFotoFinalUno.setImageBitmap(bitmap);
				tvTextoFotosFinales.setText("Pulsa en alguna foto para tomar otra nueva");
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			/*
			try {
				Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalUno);
				btnFotoFinalUno.setVisibility(View.GONE);
				vistaFotoFinalUno.setVisibility(View.VISIBLE);
				vistaFotoFinalUno.setImageBitmap(bitmap);
				tvTextoFotosFinales.setText("Pulsa en alguna foto para tomar otra nueva");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		*/
		}else if (requestCode == SOLICITUD_FOTO_FINAL_2 && resultCode == RESULT_OK) {
			
			try {
				Uri u = data.getData();
				Bitmap bitmap = decodificarBitmap(getActivity(), u);
				btnFotoFinalDos.setVisibility(View.GONE);
				vistaFotoFinalDos.setVisibility(View.VISIBLE);
				vistaFotoFinalDos.setImageBitmap(bitmap);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			
			/*
			try {
				Bitmap bitmap = decodificarBitmap(getActivity(), uriImagenFinalDos);
				btnFotoFinalDos.setVisibility(View.GONE);
				vistaFotoFinalDos.setVisibility(View.VISIBLE);
				vistaFotoFinalDos.setImageBitmap(bitmap);
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			*/
		}
		
	}
	
	private Bitmap decodificarBitmap(Context ctx, Uri uri) throws FileNotFoundException {
		int anchoMarco = 600;
		int altoMarco = 600;
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
		int fotoAncho = bmOptions.outWidth;
		int fotoAlto = bmOptions.outHeight;
		
		int escalaImagen = Math.min(fotoAncho / anchoMarco, fotoAlto / altoMarco);
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = escalaImagen;
		
		return BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
	}
	
	private void inicializarElementos() {
		tvMuestraDireccion = view.findViewById(R.id.tvUbicacion);
		btnObtenerDireccion = view.findViewById(R.id.imgButtonObtenerUbicacion);
		btnFotoInicialUno = view.findViewById(R.id.imgButtonFotoInicialUno);
		vistaFotoInicialUno = view.findViewById(R.id.imgFotoInicialUnoVista);
		btnFotoInicialDos= view.findViewById(R.id.imgButtonFotoInicialDos);
		vistaFotoInicialDos = view.findViewById(R.id.imgFotoInicialDosVista);
		tvTextoFotosIniciales = view.findViewById(R.id.tvFotosIniciales);
		linearActividades = view.findViewById(R.id.linearActividades);
		linearDirecionRecibida = view.findViewById(R.id.linearDirecionRecibida);
		linearActividades.setVisibility(View.GONE);
		linearDirecionRecibida.setVisibility(View.GONE);
		btnBuscarActividad= view.findViewById(R.id.btnBuscarActividad);
		cajaFolioActividad =view.findViewById(R.id.cajaFolioActividad);
		tvTextoFotosFinales = view.findViewById(R.id.tvFotosFinales);
		vistaFotoFinalUno = view.findViewById(R.id.imgFotoFinalUnoVista);
		vistaFotoFinalDos = view.findViewById(R.id.imgFotoFinalDosVista);
		btnFotoFinalUno = view.findViewById(R.id.imgButtonFotoFinalUno);
		btnFotoFinalDos = view.findViewById(R.id.imgButtonFotoFinalDos);
		swMostrarActividades= view.findViewById(R.id.swVerActividades);
		cajaNumActExt=view.findViewById(R.id.cajaNumActividadesExtras);
		recyclerViewActividadesExtras = view.findViewById(R.id.recyclerActividadesExtras);
		linearActividadesExtras =view.findViewById(R.id.linearActividadesExtras);
		linearActividadesExtras.setVisibility(View.GONE);
		swMostrarActividadesExtras =view.findViewById(R.id.swActividadesExtras);
		cuadroFirma=view.findViewById(R.id.firmaCuadro);
		btnGuardarFirma=view.findViewById(R.id.guardarFirmaBoton);
		btnLimpiarFirma=view.findViewById(R.id.limpiarFirmaBoton);
		btnGuardarFirma.setEnabled(false);
		btnLimpiarFirma.setEnabled(false);
	}
	
	
	
	
	
	
}
	
