package com.bringsolutions.reportes.ui.monitoreo;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import com.bringsolutions.reportes.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MonitoreoFragment extends Fragment {
	View view;
	private GoogleMap mMap;
	private SupportMapFragment mapFragment;
	
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_monitoreo, container, false);
		
		inicializarElementos();
		
		
		mapFragment.getMapAsync(new OnMapReadyCallback() {
			@Override
			public void onMapReady(GoogleMap googleMap) {
				mMap = googleMap;
				LatLng unidadUno = new LatLng(17.987315, -92.988754);
				LatLng unidadDos = new LatLng(17.998699, -92.916846);
				mMap.addMarker(new MarkerOptions().position(unidadUno).title("Unidad 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.maker_logo_digital)));
				mMap.addMarker(new MarkerOptions().position(unidadDos).title("Unidad 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.maker_logo_digital)));
				//mMap.moveCamera(CameraUpdateFactory.newLatLng(unidadUno));
				CameraUpdate animacionUbicacion = CameraUpdateFactory.newLatLngZoom(unidadUno, 12);
				mMap.animateCamera(animacionUbicacion);
			}
		});
		
		
		
		return view;
	}
	
	
	
	private void inicializarElementos() {
		//SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mapila);
		mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapila);
		
		
	}
}