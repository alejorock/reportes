package com.bringsolutions.reportes.adaptadores;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.bringsolutions.reportes.R;
import com.bringsolutions.reportes.objetos.Actividad;

import java.util.List;

public class AdaptadorActividades extends RecyclerView.Adapter<AdaptadorActividades.ViewHolder>{
	
	//clase viewholder para enlazar componesntes con la vista de los mensajes
	public static class ViewHolder extends RecyclerView.ViewHolder{
		
		private EditText descripcionActividad;
		private ImageButton btnImagenActividadExt;
		private ImageView vistaImagenActividadExt;
		
		
		public ViewHolder(View itemView) {
			super(itemView);
			//enlanzando elementos
			descripcionActividad = itemView.findViewById(R.id.cajaDescripciónActividadExtra);
			btnImagenActividadExt = itemView.findViewById(R.id.btnImagenEvidenciaActividadExtra);
			vistaImagenActividadExt = itemView.findViewById(R.id.vistaImagenEvidenciaActividadExtra);
			
			
			vistaImagenActividadExt.setVisibility(View.GONE);
		}
		
	}
	
	public List<Actividad> actividadList;
	public Context context;
	
	public AdaptadorActividades(List<Actividad> actividadLista, Context context) {
		this.actividadList = actividadLista;
		this.context = context;
	}
	
	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.tarjeta_actividad_extra,viewGroup,false);
		
		return new ViewHolder(view);
	} @Override
	
	public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final  int i) {
		//viewHolder.nombre.setText(doctoresLista.get(i).getNombre());
		
		
		
		
	}
	
	@Override
	public int getItemCount() {
		return actividadList.size();
	}
	


	

}
